package server

import (
	"context"

	"github.com/go-errors/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	pb "gitlab.com/aknudsen/error-reporting-go-grpc-service/service"
)

type Config struct {
	MandrillSecret string
	SenderEmail    string
	SenderName     string
	RecipientEmail string
	RecipientName  string
}

type Server struct {
	logger zerolog.Logger
}

func NewServer(ctx context.Context, conf Config) (*Server, error) {
	logger := log.With().Str("component", "server").Logger()

	return &Server{
		logger: logger,
	}, nil
}

func (s *Server) Fail(ctx context.Context, in *pb.Empty) (
	*pb.Empty, error) {
	s.logger.Debug().Msg("Received Fail request")
	return nil, errors.Errorf("Error!")
}

func (s *Server) DoPanic(ctx context.Context, in *pb.Empty) (
	*pb.Empty, error) {
	s.logger.Debug().Msg("Received DoPanic request")
	panic("Panic!")
}
