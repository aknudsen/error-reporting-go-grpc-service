package server

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/go-errors/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func SendBugMail(caughtErr *errors.Error, conf Config, logger zerolog.Logger) error {
	client := &http.Client{
		Timeout: time.Minute,
	}

	msg := strings.ReplaceAll(caughtErr.ErrorStack(), "\n", "<br>")
	version := "(master)"
	dateTimeStr := time.Now().UTC().String()
	body := map[string]interface{}{
		"key": conf.MandrillSecret,
		"message": map[string]interface{}{
			"html": fmt.Sprintf(`<p>%s - an error was detected in Service
%s.</p>

<pre><code>%s</code></pre>
`, dateTimeStr, version, msg),
			"subject":    fmt.Sprintf("Error Detected in Service %s", version),
			"from_email": conf.SenderEmail,
			"from_name":  conf.SenderName,
			"to": []map[string]string{
				map[string]string{
					"email": conf.RecipientEmail,
					"name":  conf.RecipientName,
					"type":  "to",
				},
			},
			"headers": map[string]string{
				"Reply-To": conf.SenderEmail,
			},
		},
	}

	var bodyJson []byte
	bodyJson, err := json.Marshal(body)
	if err != nil {
		return err
	}
	resp, err := client.Post("https://mandrillapp.com/api/1.0/messages/send.json", "application/json",
		bytes.NewReader(bodyJson))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var respBody map[string]interface{}
	if err := json.Unmarshal(b, &respBody); err == nil && respBody["status"] == "error" {
		return errors.Errorf("Mailing failed: %s", respBody["message"])
	}

	logger.Debug().Msgf("Finished sending bug report by mail")

	return nil
}

// Interceptor intercepts every gRPC request.
// If an error with either the code Unknown or Internal is returned from the handler,
// we report it by email.
func NewInterceptor(conf Config) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (resp interface{}, err error) {
		logger := log.With().Str("component", "server.interceptor").Logger()
		logger.Debug().Msgf("Processing request")

		defer func() {
			if r := recover(); r != nil {
				err = status.Errorf(codes.Internal, "panic: %s", r)
				logger.Error().Msgf("Panic detected: %s", r)
				if e := SendBugMail(errors.Wrap(err, 0), conf, logger); e != nil {
					logger.Error().Err(e).Msgf("Reporting bug by email failed")
				}
			}
		}()

		resp, err = handler(ctx, req)
		errCode := status.Code(err)
		if errCode == codes.Unknown || errCode == codes.Internal {
			logger.Debug().Err(err).Msgf("Request handler returned an internal error - reporting it")
			if e := SendBugMail(errors.Wrap(err, 0), conf, logger); e != nil {
				logger.Error().Err(e).Msgf("Reporting bug by email failed")
			}

			return
		}

		logger.Debug().Msgf("Request finished successfully")
		return
	}
}
