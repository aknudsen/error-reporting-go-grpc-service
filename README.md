# Error Reporting Go gRPC Service
This is an example of a gRPC service written in Go that automatically reports non-user
errors and panics by email. It requires that you have a [Mandrill](https://mandrillapp.com/)
account for sending mail.

## Requirements
* Go 1.12+
* [Mage](https://magefile.org)
* A [Mandrill](https://mandrillapp.com/) account

## Building
To build the service, simply invoke `mage`.

## Testing
First make a configuration file config.toml, by copying config.toml.example and filling in
the values. Then start the server: `./build/service server`. Once the server is running,
you can test reporting of both errors and panics like so:

```
./build/service fail
./build/service panic
```

The first of the above two commands will cause an error on the server, which should be reported
to your mail address, and the second one a panic which should also be reported.
