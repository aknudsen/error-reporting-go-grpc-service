package main

import (
	"gitlab.com/aknudsen/error-reporting-go-grpc-service/cmd"
)

func main() {
	cmd.Execute()
}
