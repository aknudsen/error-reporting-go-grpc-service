module gitlab.com/aknudsen/error-reporting-go-grpc-service

go 1.12

require (
	github.com/burntsushi/toml v0.3.1
	github.com/go-errors/errors v1.0.1
	github.com/golang/protobuf v1.3.1
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/magefile/mage v1.8.0
	github.com/rs/zerolog v1.14.3
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	golang.org/x/net v0.0.0-20190328230028-74de082e2cca // indirect
	google.golang.org/appengine v1.4.0 // indirect
	google.golang.org/genproto v0.0.0-20190418145605-e7d98fc518a7 // indirect
	google.golang.org/grpc v1.20.1
)
