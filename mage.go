// +build mage

package main

import (
	"github.com/magefile/mage/sh"
)

func Build() error {
	if err := sh.Run("protoc", "-Iservice", "--go_out=plugins=grpc:service",
		"service/service.proto"); err != nil {
		return err
	}
	if err := sh.Run("go", "build", "-o", "build/service"); err != nil {
		return err
	}

	return nil
}

func Lint() error {
	if err := sh.Run("golangci-lint", "run"); err != nil {
		return err
	}

	return nil
}

func Docs() error {
	if err := sh.Run("asciidoctor", "-D", "public", "docs/index.adoc"); err != nil {
		return err
	}

	return nil
}

var Default = Build
