package cmd

import (
	"context"
	"fmt"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	pb "gitlab.com/aknudsen/error-reporting-go-grpc-service/service"
	"google.golang.org/grpc"
)

var cmdPanic = &cobra.Command{
	Use:   "panic",
	Short: "Panic will cause a panic on the server",
	RunE: func(cmd *cobra.Command, args []string) error {
		ctx := context.Background()

		address := fmt.Sprintf("localhost:%d", port)
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			return err
		}
		defer conn.Close()
		c := pb.NewServiceClient(conn)

		log.Info().Msgf("Calling service")
		_, _ = c.DoPanic(ctx, &pb.Empty{})
		log.Info().Msgf("Called service successfully")

		return nil
	},
}

func init() {
	rootCmd.AddCommand(cmdPanic)
}
