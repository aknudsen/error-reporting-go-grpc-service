package cmd

import (
	"context"
	"fmt"
	"net"

	"github.com/burntsushi/toml"
	"github.com/go-errors/errors"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/aknudsen/error-reporting-go-grpc-service/server"
	pb "gitlab.com/aknudsen/error-reporting-go-grpc-service/service"
	"google.golang.org/grpc"
)

func readConf() (server.Config, error) {
	var conf server.Config
	_, err := toml.DecodeFile("config.toml", &conf)
	if conf.MandrillSecret == "" {
		return conf, errors.Errorf("mandrill-secret must be set in configuration")
	}
	if conf.SenderEmail == "" {
		return conf, errors.Errorf("sender-email must be set in configuration")
	}
	if conf.SenderName == "" {
		return conf, errors.Errorf("sender-name must be set in configuration")
	}
	if conf.RecipientEmail == "" {
		return conf, errors.Errorf("recipient-email must be set in configuration")
	}
	if conf.RecipientName == "" {
		return conf, errors.Errorf("recipient-name must be set in configuration")
	}

	return conf, err
}

var cmdServer = &cobra.Command{
	Use:   "server",
	Short: "Server",
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		conf, err := readConf()
		if err != nil {
			return
		}

		logger := log.With().Str("component", "app").Logger()

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		srv, err := server.NewServer(ctx, conf)
		if err != nil {
			logger.Error().Err(err).Msgf("Failed to create server")
			return
		}

		l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
		if err != nil {
			return
		}
		s := grpc.NewServer(grpc.UnaryInterceptor(server.NewInterceptor(conf)))

		pb.RegisterServiceServer(s, srv)
		logger.Info().Msgf("listening on port %d...", port)
		if err = s.Serve(l); err != nil {
			return
		}

		return
	},
}

func init() {
	rootCmd.AddCommand(cmdServer)
}
